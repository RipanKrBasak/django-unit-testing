from django.shortcuts import render, reverse, redirect, get_object_or_404
from django.http import Http404, HttpResponseRedirect
from .models import Question, Choice

# Create your views here.


def home_page(request):

    return render(request, 'home.html', context=None)


def poll_page(request):
    latest_question_list = Question.objects.order_by('-pub_date')
    context = {
        'latest_question_list': latest_question_list
    }

    return render(request, 'poll.html', context=context)


def detail_page(request, question_id):

    try:
        question = Question.objects.get(pk=question_id)
        context = {
            'question': question
        }
    except Question.DoesNotExist:
        raise Http404("Question Does Not exist")

    return render(request, 'detail.html', context=context)

    # return render(request,'detail.html',context=None)


def result_page(request, question_id):

    question = get_object_or_404(Question, pk=question_id)

    return render(request, 'results.html', {'question': question})


def vote_page(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        if request.method == 'post':
            selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        # Redisplay the question voting form.
        return render(request, 'detail.html', {
            'question': question,
            'error_message': "You didn't select a choice.",
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()

        return redirect(reverse('polls:result', args=[question_id]))
