from django.test import TestCase, Client
from django.urls import reverse, resolve
from polls.models import Question, Choice
from django.utils import timezone
from polls import views
from polls.models import Question, Choice


class TestViews(TestCase):

    def setUp(self):
        self.client = Client()
        self.poll_url = reverse('polls:poll')
        self.detail_url = reverse('polls:detail', args=[1])
        self.result_url = reverse('polls:result', args=[1])
        # q = Question(question_text="What's new?", pub_date=timezone.now())

        self.q1 = Question.objects.create(
            question_text="Qustion 1", pub_date=timezone.now())
        self.q2 = Question.objects.create(
            question_text="Qustion 2", pub_date=timezone.now())
        self.c1 = Choice.objects.create(
            question=self.q1,
            choice_text="Choice 1 -> Question 1")
        self.c2 = Choice.objects.create(
            question=self.q1,
            choice_text="Choice 2-> Qeustion 1")

        # return super().setUp()
        # print("***",q.id)

    def test_poll_list_GET(self):

        response = self.client.get(self.poll_url)

        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'poll.html')

    def test_detail_list_GET(self):

        response = self.client.get(self.detail_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'detail.html')

    def test_vote_page_POST_select_choice(self):
        c1 = self.q1.choice_set.get(pk=1)
        c1.votes += 1
        self.assertEquals(c1.votes, 1)
        self.assertEquals(self.q1, self.c1.question)
        self.assertEquals(c1.choice_text, self.c1.choice_text)

    def test_vote_page_redirect_to_result_page(self):
        response = self.client.get(self.result_url)
        self.assertEquals(response.status_code,302)

    # def test_vote_page_redirect(self):
    #     print("****",self.result_url)
    #     response = self.client.get(self.result_url)
    #     self.assertEquals(response.status_code, 200)

    #     # self.assertTemplateUsed(response, 'results.html')
    #     self.assertRedirects(response, self.result_url, status_code=302,
    #                          target_status_code=200, fetch_redirect_response=True)
