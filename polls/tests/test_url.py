from django.test import SimpleTestCase
from django.urls import reverse, resolve
from polls import views


class TestHomePageUrl(SimpleTestCase):
    def test_home_page_status_code(self):
        # return 200
        url = '/'
        response = self.client.get(url)
        self.assertEquals(response.status_code, 200)

    def test_view_url_by_name(self):
        # /some/url
        url = reverse('polls:home')
        response = self.client.get(url)
        self.assertEquals(response.status_code, 200)

    def test_match_url_func_with_view(self):
        url = reverse('polls:home')
        self.assertEquals(resolve(url).func, views.home_page)


    def test_home_page_template_match(self):
        url = reverse('polls:home')
        response = self.client.get(url)
        self.assertTemplateUsed(response,'home.html')
        
        

class TestDetailPageUrl(SimpleTestCase):
   

    def test_view_detail_url_by_name(self):
        # /some/url
        url = reverse('polls:detail',args=[1])
        response = self.client.get(url)
        self.assertEquals(response.status_code, 200)

    # def test_match_detail_url_func_with_view(self):
    #     url = reverse('polls:detail',args=[1])
    #     self.assertEquals(resolve(url).func, views.detail_page)


    # def test_detail_page_template_match(self):
    #     url = reverse('polls:detail',args=[1])
    #     response = self.client.get(url)
    #     self.assertTemplateUsed(response,'polls:detail.html')
        
class TestResultPageUrl(SimpleTestCase):
    global url
    # url = reverse('polls:result',args=[1])

       

    def test_view_result_url_by_name(self):
        # /some/url
        url = reverse('polls:result',args=[1])
        response = self.client.get(url)
        self.assertEquals(response.status_code, 200)

    def test_match_result_url_func_with_view(self):
        url = reverse('polls:result',args=[1])
        self.assertEquals(resolve(url).func, views.result_page)


    def test_result_page_template_match(self):
        url = reverse('polls:result',args=[1])
        response = self.client.get(url)
        self.assertTemplateUsed(response,'results.html')
        
        
class TestVotePageUrl(SimpleTestCase):
       

    def test_view_vote_url_by_name(self):
        # /some/url
        url = reverse('polls:vote',args=[1])
        response = self.client.get(url)
        self.assertEquals(response.status_code, 200)

    def test_match_vote_url_func_with_view(self):
        url = reverse('polls:vote',args=[1])
        self.assertEquals(resolve(url).func, views.vote_page)


    def test_vote_page_template_match(self):
        url = reverse('polls:vote',args=[1])
        response = self.client.get(url)
        self.assertTemplateUsed(response,'vote.html')
        
        



