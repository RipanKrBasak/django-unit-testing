from django.test import TestCase
from django.utils import timezone
from polls.models import Question,Choice

class TestQuestionModel(TestCase):
    def setUp(self):
        # Create Questions
        Question.objects.create(question_text='Question 1',pub_date=timezone.now())
        Question.objects.create(question_text='Question 2',pub_date=timezone.now())
    
    # def tearDown(self):
    #     print("tear Down")
    
    def test_create_question_model(self):
        q1 = Question.objects.get(pk=1)
        q2 = Question.objects.get(pk=2)
        text1 = q1.question_text
        text2 = q2.question_text
        
        
        self.assertEquals(text1,'Question 1')
        self.assertEquals(text2,'Question 2')
class TestChoiceModel(TestCase):
    def setUp(self):
        # Create Questions
        Question.objects.create(question_text='Question 1',pub_date=timezone.now())
        Question.objects.create(question_text='Question 2',pub_date=timezone.now())
    
    # def tearDown(self):
    #     print("tear Down")
    
    def test_create_choice_model(self):
       
        q1 = Question.objects.get(pk=1)
        q2 = Question.objects.get(pk=2)
        choice1 = Choice.objects.create(question=q1,choice_text='choice 1', votes=0)
        # text1 = q1.question_text
        # text2 = q2.question_text
        
        
        self.assertEquals(choice1.question,q1)
        self.assertEquals(choice1.choice_text,'choice 1')
        # self.assertEquals(text2,'Question 2')