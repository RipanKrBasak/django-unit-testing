
from django.urls import path
from polls import views
app_name = 'polls'
urlpatterns = [
    path('', views.home_page, name='home'),
    path('poll/', views.poll_page, name='poll'),
    path('poll/<int:question_id>/', views.detail_page, name='detail'),
    path('poll/<int:question_id>/result/', views.result_page, name='result'),
    path('poll/<int:question_id>/vote/', views.vote_page, name='vote'),
]
